set -eu

RED='\033[0;31m'
NC='\033[0m'
RELEASE_NAME="${APP_NAME}-${ENVIRONMENT}"
echo "Using CONTAINER_TAG: ${CONTAINER_TAG}"

echo "Configure kubernetes deploy context"
kubectl config set-credentials user --token=${K8S_TOKEN}
kubectl config set-cluster cluster --server=${K8S_CLUSTER} --insecure-skip-tls-verify=true
kubectl config set-context deploy-ctx --cluster=cluster --user=user --namespace ${NAMESPACE}
kubectl config use-context deploy-ctx

function tillerless() {
    helm tiller run ${NAMESPACE} -- $@
}

function get_current_version() {
    tillerless helm list | awk "/$1/{print \$2}"
}

CURRENT_VERSION=`get_current_version "$RELEASE_NAME"`


function rollback() {
    echo "Service deploy failed."
    echo "Rolling back to  $CURRENT_VERSION ..."

    tillerless helm rollback --force --wait "$RELEASE_NAME" "$CURRENT_VERSION"

    rollback_status=$?

    if [[ $rollback_status -ne 0 ]]; then
      echo -e "${RED}Could not rollback...${NC}"

      set -e
      return $rollback_status
    fi

    set -e
    return $deploy_status

}

function upgrade() {
    echo "Upgrade for release: ${RELEASE_NAME} Container tag: ${CONTAINER_TAG}"

    helm template \
        ${CHART_PATH} \
        --set image.tag=${CONTAINER_TAG} \
        -n ${RELEASE_NAME} \
        -f ${CHART_PATH}/values/${ENVIRONMENT}/values.yaml


    tillerless helm upgrade \
        --install \
        --debug \
        --wait \
        --namespace=${NAMESPACE} \
        --set image.tag=${CONTAINER_TAG} \
        ${RELEASE_NAME} \
        ${CHART_PATH} \
        -f ${CHART_PATH}/values/${ENVIRONMENT}/values.yaml

    deploy_status=$?

    if [[ $deploy_status -ne 0 ]]; then
        rollback
    fi;

}

upgrade;