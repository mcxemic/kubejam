{{/* vim: set filetype=mustache: */}}
{{/*
*/}}
{{- define "pebook.name" -}}
{{- printf "%s" .Values.app.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "pebook.release" -}}
{{- printf "%s" .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "deployment.labels" }}
app: {{ include "pebook.name" . }}
release: {{ include "pebook.release" . }}
deploy: {{ .Values.app.deploy }}
{{- end }}
