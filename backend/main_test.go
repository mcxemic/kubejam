package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func HealthCheckHandlerTest(t *testing.T, w http.ResponseWriter, r *http.Request) {
	req, err := http.NewRequest("GET", "/healthcheck", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(healthCheckHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong response status: got %v expected %v",
			status, http.StatusOK)
	}

	expected := `{"status": ok}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
